import Vue from 'vue'
import Router from 'vue-router'
import Home from '../view/home.vue'
import Login from '../view/Login/Login.vue'
import Main from '../view/Main/Main'
import guaranty from '../view/guaranty/guaranty.vue'
import risk from '../view/loan/risk.vue'
import repayment from '../view/loanmanage/repayment.vue'
import contractinfo from '../view/loan/contractinfo.vue'
import epinfo from '../view/epinfo/epinfo.vue'
import epdetailpage from '../view/epinfo/epdetailpage.vue'
import repaymentregistered from '../view/loanmanage/repaymentregistered.vue'
import overduecollection from '../view/loanmanage/overduecollection.vue'
import preloan from '../view/preloan/preloan.vue';
import info from '../view/loan/info.vue'
import infos from '../view/contract/infos.vue'
import history from '../view/loan/history.vue'
import process from '../view/loan/process.vue'
import appendix from '../view/loan/appendix.vue'

import preloans from '../view/contract/preloans.vue'

import approval from '../view/guaranty/approval.vue'

import file from '../view/contract/file.vue'
import detailed from '../view/contract/detailed.vue'
import historyapproval from '../view/contract/historyapproval.vue'
import law from '../view/loan/law.vue'
import infoss from '../view/loanmanage/infoss.vue'
import collectionrecords from '../view/loanmanage/collectionrecords.vue'
import assetpreservation from '../view/loanmanage/assetpreservation.vue'
import Loanlevelfive from '../view/loanmanage/Loanlevelfive.vue'
import finance from '../view/loan/finance.vue'
import money from '../view/loan/money.vue'
import blacklist from '../view/epinfo/blacklist.vue'
import borrowDemand from '../view/preloan/borrowDemand.vue'


Vue.use(Router)
// 解决重复点击路由报错的BUG
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}



let router = new Router({
  mode: 'history',
  //   base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login

    },
    {
      path: '/login',
      name: 'Login',
      component: Login

    },
    {
      path: '/Main',
      name: 'Main',
      component: Main
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home,
      children: [
        {
          path: "/guaranty",
          name: "guaranty",
          component: guaranty
        },
        {
          path: "/risk",
          name: "risk",
          component: risk
        }, {
          path: "/approval",
          name: "/approval",
          component: approval
        }
        ,
        {
          path: "/repayment",
          name: "repayment",
          component: repayment,
        },
        {
          path: "/repaymentregistered",
          name: "repaymentregistered",
          component: repaymentregistered
        }
        , {
          path: "/law",
          name: "law",
          component: law
        }
        , {
          path: "/repayment",
          name: "repayment",
          component: repayment
        }, {
          path: "/finance",
          name: "finance",
          component: finance
        },
        {
          path: "/money",
          name: "money",
          component: money
        },
        {
          path: "/contractinfo",
          name: "contractinfo",
          component: contractinfo,
          children: [
            {
              path: "/info",
              name: "info",
              component: info
            },
            {
              path: "/process",
              name: "process",
              component: process
            },
            {
              path: "/history",
              name: "history",
              component: history
            },
            {
              path: "/appendix",
              name: "appendix",
              component: appendix
            }
          ]
        },
        {
          path: "/repayment",
          name: "repayment",
          component: repayment
        },
        {
          path: "/preloans",
          name: "preloans",
          component: preloans
        },
       
        {
          path: "/epinfo",
          name: "epinfo",
          component: epinfo
        },
        {
          path: "/epdetailpage",
          name: "epdetailpage",
          component: epdetailpage
        },
        {
          path: "/blacklist",
          name: "blacklist",
          component: blacklist
        },

        {
          path: "/repaymentregistered",
          name: "repaymentregistered",
          component: repaymentregistered
        },
        {
          path: "/collectionrecords",
          name: "collectionrecords",
          component: collectionrecords
        },

        {
          path: "/Loanlevelfive",
          name: "Loanlevelfive",
          component: Loanlevelfive
        },

        {
          path: "/overduecollection",
          name: "overduecollection",
          component: overduecollection
        },
        {
          path: "/assetpreservation",
          name: "assetpreservation",
          component: assetpreservation
        },
       
        {
          path: "/infoss",
          name: "infoss",
          component: infoss
        },
        {
          path: "/preloan",
          name: "preloan",
          component: preloan

        },
        {
          path: "/borrowDemand",
          name: "borrowDemand",
          component: borrowDemand

         },
         {
          path: "/infos",
          name: "infos",
          component: infos,
          children:[
            {
              path: "/detailed",
              name: "detailed",
              component: detailed
            },
            {
              path: "/historyapproval",
              name: "historyapproval",
              component: historyapproval
            },
            {
              path: "/file",
              name: "file",
              component: file
            },

          ]
            },



         


        

      ]
    
    }
    ]
})
router.beforeEach((to, from, next) => {
  console.log("路由前");
  //如果是访问登录页，直接放行
  if (to.path == '/' || to.path == '/login') {
    next();
    return;
  }
  //否则判断是否有token
  let token = sessionStorage.getItem("token");
  if (token) {
    //有放行

    next();
  } else {
    //没有
    alert("你还没登录!");
    //去登录页面
    next("/login");
  }

})
//后置钩子
router.afterEach((to, from) => {
  console.log("路由后");

});

export default router
