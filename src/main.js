// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import JsonExcel from 'vue-json-excel'

Vue.component('downloadExcel', JsonExcel)
Vue.config.productionTip = false
Vue.component('downloadExcel', JsonExcel)
Vue.prototype.$axios = axios
Vue.use(ElementUI)

let http=axios.create() //创建axios对象
http.interceptors.request.use(
  config=>{
    //发送
    console.log("发送请求之前执行")
//从sessdionStoage 中添加到请求头中
let token=window.sessionStorage.getItem("token");
let refresh=window.sessionStorage.getItem("refresh");
config.headers.authentication=token;
config.headers.refresh=refresh;
    return config;
  }
   
);
//响应拦截器
http.interceptors.response.use(
  response=>{
    //发送
    console.log("响应接受")
   //得到Token 存到本地
  let token= response.headers.authentication;
  let refresh=response.headers.refresh;
  let perms=response.headers.perms;
  let code=response.data.code;
//判断是否为空
if(token !=null ){
  //永久存在
//  window.localStorage.setItem("token",token);
 window.sessionStorage.setItem("token",token);

}
if(refresh != null){
  window.sessionStorage.setItem("refresh",refresh);
 }
if(code == 201){
  window.location.href="/login"
}
    return response;
  }
);


Vue.config.productionTip = false
//全局
Vue.prototype.$axios=http

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})





